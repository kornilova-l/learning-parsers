package com.github.kornilova203;

/**
 * @author Liudmila Kornilova
 **/
public class TestUtil {
    public static String toString(Node node, int level) {
        StringBuilder sb = new StringBuilder(" ".repeat(level * 2))
                .append(node.elementType.toString());
        for (Node child : node.children) {
            sb.append("\n");
            sb.append(toString(child, level + 1));
        }
        return sb.toString();
    }
}
