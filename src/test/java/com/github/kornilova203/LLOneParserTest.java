package com.github.kornilova203;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.github.kornilova203.Terminals.*;
import static org.junit.Assert.assertEquals;

/**
 * @author Liudmila Kornilova
 **/
public class LLOneParserTest {
    @Test
    public void testSimple() throws ParserException, IOException {
        Lexer lexer = new Lexer(new ElementType[]{SELECT, IDENTIFIER, FROM, IDENTIFIER, END});
        Node node = new LLOneParser().parse(lexer);
        String expected = Files.readString(getPath("simple.txt"));
        assertEquals(expected, TestUtil.toString(node, 0));
    }

    @Test
    public void testMultipleColumns() throws ParserException, IOException {
        Lexer lexer = new Lexer(new ElementType[]{SELECT, IDENTIFIER, COMMA, IDENTIFIER, COMMA, IDENTIFIER, FROM, IDENTIFIER, END});
        Node node = new LLOneParser().parse(lexer);
        String expected = Files.readString(getPath("multipleColumns.txt"));
        assertEquals(expected, TestUtil.toString(node, 0));
    }

    @Test(expected = ParserException.class)
    public void testParserException() throws ParserException {
        Lexer lexer = new Lexer(new ElementType[]{SELECT, IDENTIFIER, COMMA, IDENTIFIER, COMMA, FROM, IDENTIFIER, END});
        new LLOneParser().parse(lexer);
    }

    @Test
    public void testWhere() throws ParserException, IOException {
        Lexer lexer = new Lexer(new ElementType[]{SELECT, IDENTIFIER, FROM, IDENTIFIER, WHERE, IDENTIFIER, COMPARE_OP, NUMBER, END});
        Node node = new LLOneParser().parse(lexer);
        String expected = Files.readString(getPath("where.txt"));
        assertEquals(expected, TestUtil.toString(node, 0));
    }

    @Test
    public void testWhereWithAnd() throws ParserException, IOException {
        Lexer lexer = new Lexer(new ElementType[]{SELECT, IDENTIFIER, FROM, IDENTIFIER, WHERE, IDENTIFIER, COMPARE_OP, NUMBER, AND_OP, IDENTIFIER, COMPARE_OP, NUMBER, END});
        Node node = new LLOneParser().parse(lexer);
        String expected = Files.readString(getPath("whereWithAnd.txt"));
        assertEquals(expected, TestUtil.toString(node, 0));
    }

    private Path getPath(String name) {
        return Path.of("src/test/resources/LL", name);
    }
}
