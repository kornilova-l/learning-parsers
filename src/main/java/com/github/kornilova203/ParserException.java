package com.github.kornilova203;

/**
 * @author Liudmila Kornilova
 **/
public class ParserException extends Exception {
    public ParserException(String message) {
        super(message);
    }
}
