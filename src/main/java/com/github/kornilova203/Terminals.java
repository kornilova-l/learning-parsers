package com.github.kornilova203;

/**
 * @author Liudmila Kornilova
 **/
public interface Terminals {
    ElementType SELECT = new ElementType("SELECT");
    ElementType IDENTIFIER = new ElementType("IDENTIFIER");
    ElementType COMMA = new ElementType("COMMA");
    ElementType FROM = new ElementType("FROM");
    ElementType WHERE = new ElementType("WHERE");
    ElementType AND_OP = new ElementType("AND_OP");
    ElementType COMPARE_OP = new ElementType("COMPARE_OP");
    ElementType NUMBER = new ElementType("NUMBER");
    ElementType END = new ElementType("$");
}
