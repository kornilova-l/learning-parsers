package com.github.kornilova203;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Liudmila Kornilova
 **/
public class Node {
    public final ElementType elementType;
    public final List<Node> children;

    public Node(ElementType elementType, List<Node> children) {
        this.elementType = elementType;
        this.children = children;
    }

    public static Node terminal(ElementType elementType) {
        return new Node(elementType, new ArrayList<>());
    }


    @Override
    public String toString() {
        return elementType.toString();
    }
}
