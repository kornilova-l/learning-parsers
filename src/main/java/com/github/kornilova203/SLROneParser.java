package com.github.kornilova203;

import java.util.*;

import static com.github.kornilova203.Node.terminal;
import static com.github.kornilova203.NonTerminals.*;
import static com.github.kornilova203.Terminals.*;
import static java.util.Collections.reverse;

/**
 * @author Liudmila Kornilova
 **/
public class SLROneParser {
    private static final List<Map<ElementType, Action>> table = new ArrayList<>();

    static {
        add(table, 0, SELECT, new Shift(2));
        add(table, 0, QUERY, new Shift(1));
        add(table, 1, END, Accept.INSTANCE);
        add(table, 2, IDENTIFIER, new Shift(3));
        add(table, 2, COLUMN_LIST, new Shift(4));
        add(table, 3, FROM, new Reduce(COLUMN_LIST, 1));
        add(table, 3, COMMA, new Reduce(COLUMN_LIST, 1));
        add(table, 4, COMMA, new Shift(7));
        add(table, 4, FROM, new Shift(5));
        add(table, 4, FROM_CLAUSE, new Shift(9));
        add(table, 5, IDENTIFIER, new Shift(6));
        add(table, 6, WHERE, new Reduce(FROM_CLAUSE, 2));
        add(table, 6, END, new Reduce(FROM_CLAUSE, 2));
        add(table, 7, IDENTIFIER, new Shift(8));
        add(table, 8, FROM, new Reduce(COLUMN_LIST, 3));
        add(table, 8, COMMA, new Reduce(COLUMN_LIST, 3));
        add(table, 9, WHERE, new Shift(11));
        add(table, 9, END, new Reduce(WHERE_CLAUSE, 0));
        add(table, 9, WHERE_CLAUSE, new Shift(10));
        add(table, 10, END, new Reduce(QUERY, 4));
        add(table, 11, IDENTIFIER, new Shift(13));
        add(table, 11, NUMBER, new Shift(14));
        add(table, 11, PRIMARY_EXPR, new Shift(12));
        add(table, 11, AND_EXPR, new Shift(16));
        add(table, 11, COMPARE_EXPR, new Shift(15));
        add(table, 12, AND_OP, new Reduce(COMPARE_EXPR, 1));
        add(table, 12, COMPARE_OP, new Reduce(COMPARE_EXPR, 1));
        add(table, 12, END, new Reduce(COMPARE_EXPR, 1));
        add(table, 13, AND_OP, new Reduce(PRIMARY_EXPR, 1));
        add(table, 13, COMPARE_OP, new Reduce(PRIMARY_EXPR, 1));
        add(table, 13, END, new Reduce(PRIMARY_EXPR, 1));
        add(table, 14, AND_OP, new Reduce(PRIMARY_EXPR, 1));
        add(table, 14, COMPARE_OP, new Reduce(PRIMARY_EXPR, 1));
        add(table, 14, END, new Reduce(PRIMARY_EXPR, 1));
        add(table, 15, AND_OP, new Reduce(AND_EXPR, 1));
        add(table, 15, COMPARE_OP, new Shift(19));
        add(table, 15, END, new Reduce(AND_EXPR, 1));
        add(table, 16, AND_OP, new Shift(17));
        add(table, 16, END, new Reduce(WHERE_CLAUSE, 2));
        add(table, 17, IDENTIFIER, new Shift(13));
        add(table, 17, NUMBER, new Shift(14));
        add(table, 17, PRIMARY_EXPR, new Shift(12));
        add(table, 17, COMPARE_EXPR, new Shift(18));
        add(table, 18, AND_OP, new Reduce(AND_EXPR, 3));
        add(table, 18, COMPARE_OP, new Shift(19));
        add(table, 18, END, new Reduce(AND_EXPR, 3));
        add(table, 19, IDENTIFIER, new Shift(13));
        add(table, 19, NUMBER, new Shift(14));
        add(table, 19, PRIMARY_EXPR, new Shift(20));
        add(table, 20, AND_OP, new Reduce(COMPARE_EXPR, 3));
        add(table, 20, COMPARE_OP, new Reduce(COMPARE_EXPR, 3));
        add(table, 20, END, new Reduce(COMPARE_EXPR, 3));
    }


    public Node parse(Lexer lexer) throws ParserException {
        lexer.advance();
        Deque<NodeAndState> stack = new ArrayDeque<>();
        stack.push(new NodeAndState(null, 0));
        while (true) {
            NodeAndState item = stack.peek();
            assert item != null;
            ElementType token = lexer.getToken();
            Action action = getAction(token, item.state);
            if (action instanceof Shift) {
                stack.push(new NodeAndState(terminal(token), ((Shift) action).state));
                lexer.advance();
            } else if (action instanceof Reduce) {
                Reduce reduce = (Reduce) action;
                List<Node> children = new ArrayList<>();
                for (int i = 0; i < reduce.nodesCount; i++) {
                    NodeAndState child = stack.pop();
                    children.add(child.node);
                }
                reverse(children);
                int state = stack.peek().state;
                int newState = ((Shift) getAction(reduce.elementType, state)).state;
                stack.push(new NodeAndState(new Node(reduce.elementType, children), newState));
            } else {
                assert action == Accept.INSTANCE;
                Node node = stack.peek().node;
                assert node.elementType == QUERY;
                return node;
            }
        }
    }

    private Action getAction(ElementType type, int state) throws ParserException {
        Map<ElementType, Action> row = table.get(state);
        Action action = row.get(type);
        if (action == null) {
            StringBuilder sb = new StringBuilder();
            boolean first = true;
            for (ElementType elementType : row.keySet()) {
                if (first) first = false;
                else sb.append(", ");
                sb.append(elementType.toString());
            }
            throw new ParserException(String.format("State: %d. Expected one of: %s got: %s", state, sb, type));
        }
        return action;
    }

    private static void add(List<Map<ElementType, Action>> table, int state, ElementType elementType, Action action) {
        while (state >= table.size()) {
            table.add(new HashMap<>());
        }
        Map<ElementType, Action> row = table.get(state);
        row.put(elementType, action);
    }

    private interface Action {
    }

    private static class Shift implements Action {
        final int state;

        private Shift(int state) {
            this.state = state;
        }

        @Override
        public String toString() {
            return "Shift{" +
                    "state=" + state +
                    '}';
        }
    }

    private static class Reduce implements Action {
        final ElementType elementType;
        final int nodesCount;

        private Reduce(ElementType elementType, int nodesCount) {
            this.elementType = elementType;
            this.nodesCount = nodesCount;
        }

        @Override
        public String toString() {
            return "Reduce{" +
                    "elementType=" + elementType +
                    ", nodesCount=" + nodesCount +
                    '}';
        }
    }

    private static class Accept implements Action {
        final static Accept INSTANCE = new Accept();

        private Accept() {
        }
    }

    private static class NodeAndState {
        final Node node;
        final int state;

        private NodeAndState(Node node, int state) {
            this.node = node;
            this.state = state;
        }

        @Override
        public String toString() {
            return "NodeStackItem{" +
                    "node=" + node +
                    ", state=" + state +
                    '}';
        }
    }
}
