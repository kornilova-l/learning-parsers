package com.github.kornilova203;

/**
 * @author Liudmila Kornilova
 **/
public interface NonTerminals {
    ElementType QUERY = new ElementType("QUERY");
    ElementType COLUMN_LIST = new ElementType("COLUMN_LIST");
    ElementType COLUMN_LIST_TAIL = new ElementType("COLUMN_LIST_TAIL");
    ElementType FROM_CLAUSE = new ElementType("FROM_CLAUSE");
    ElementType WHERE_CLAUSE = new ElementType("WHERE_CLAUSE");
    ElementType AND_EXPR = new ElementType("AND_EXPR");
    ElementType AND_EXPR_1 = new ElementType("AND_EXPR_1");
    ElementType COMPARE_EXPR = new ElementType("COMPARE_EXPR");
    ElementType COMPARE_EXPR_1 = new ElementType("COMPARE_EXPR_1");
    ElementType PRIMARY_EXPR = new ElementType("PRIMARY_EXPR");
}
