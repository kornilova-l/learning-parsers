package com.github.kornilova203;

/**
 * @author Liudmila Kornilova
 **/
public class Lexer {
    private final ElementType[] elementTypes;
    private int current = -1;

    public Lexer(ElementType[] elementTypes) {
        this.elementTypes = elementTypes;
    }

    public ElementType getToken() {
        return elementTypes[current];
    }

    public void advance() {
        current++;
    }

    public int getCurrentPosition() {
        return current;
    }
}
