package com.github.kornilova203;

import java.util.ArrayList;
import java.util.List;

import static com.github.kornilova203.Node.terminal;

/**
 * QUERY            -> select COLUMN_LIST FROM_CLAUSE WHERE_CLAUSE
 * COLUMN_LIST      -> identifier COLUMN_LIST_TAIL
 * private COLUMN_LIST_TAIL -> comma identifier COLUMN_LIST_TAIL | ε
 * FROM_CLAUSE      -> from identifier
 * WHERE_CLAUSE     -> where AND_EXPR | ε
 * AND_EXPR         -> COMPARE_EXPR AND_EXPR_1
 * private AND_EXPR_1       -> and_op COMPARE_EXPR AND_EXPR_1 | ε
 * COMPARE_EXPR     -> PRIMARY_EXPR COMPARE_EXPR_1
 * private COMPARE_EXPR_1   -> compare_op PRIMARY_EXPR COMPARE_EXPR_1 | ε
 * PRIMARY_EXPR     -> identifier | number
 */
public class LLOneCompactParser {
    public Node parse(Lexer lexer) throws ParserException {
        lexer.advance();
        return parseQuery(lexer);
    }

    /**
     * QUERY -> select COLUMN_LIST FROM_CLAUSE WHERE_CLAUSE
     */
    private Node parseQuery(Lexer lexer) throws ParserException {
        ElementType current = lexer.getToken();
        List<Node> children = new ArrayList<>();
        if (current == Terminals.SELECT) {
            children.add(terminal(Terminals.SELECT));
            lexer.advance();
            children.add(parseColumnList(lexer));
            children.add(parseFromClause(lexer));
            Node whereClause = parseWhereClause(lexer);
            if (whereClause != null) children.add(whereClause);
        } else {
            throw new ParserException(String.format("%s expected: %d", Terminals.SELECT, lexer.getCurrentPosition()));
        }
        return new Node(NonTerminals.QUERY, children);
    }

    /**
     * COLUMN_LIST -> identifier COLUMN_LIST_TAIL
     */
    private Node parseColumnList(Lexer lexer) throws ParserException {
        ElementType current = lexer.getToken();
        List<Node> children = new ArrayList<>();
        if (current == Terminals.IDENTIFIER) {
            children.add(terminal(Terminals.IDENTIFIER));
            lexer.advance();
            children.addAll(parseColumnListTail(lexer));
        } else {
            throw new ParserException(String.format("%s expected: %d", Terminals.IDENTIFIER, lexer.getCurrentPosition()));
        }
        return new Node(NonTerminals.COLUMN_LIST, children);
    }

    /**
     * private COLUMN_LIST_TAIL -> comma identifier COLUMN_LIST_TAIL | ε
     */
    private List<Node> parseColumnListTail(Lexer lexer) throws ParserException {
        ElementType current = lexer.getToken();
        List<Node> children = new ArrayList<>();
        if (current == Terminals.COMMA) {
            children.add(terminal(Terminals.COMMA));
            lexer.advance();
            current = lexer.getToken();
            if (current != Terminals.IDENTIFIER) {
                throw new ParserException(String.format("%s expected: %d", Terminals.IDENTIFIER, lexer.getCurrentPosition()));
            }
            children.add(terminal(Terminals.IDENTIFIER));
            lexer.advance();
            children.addAll(parseColumnListTail(lexer));
        } else if (current == Terminals.FROM) {
            return children;
        } else {
            throw new ParserException(String.format("%s or %s expected: %d", Terminals.COMMA, Terminals.FROM, lexer.getCurrentPosition()));
        }
        return children;
    }

    /**
     * FROM_CLAUSE -> from identifier
     */
    private Node parseFromClause(Lexer lexer) throws ParserException {
        ElementType current = lexer.getToken();
        List<Node> children = new ArrayList<>();
        if (current == Terminals.FROM) {
            children.add(terminal(Terminals.FROM));
            lexer.advance();
            current = lexer.getToken();
            if (current != Terminals.IDENTIFIER) {
                throw new ParserException(String.format("%s expected: %d", Terminals.IDENTIFIER, lexer.getCurrentPosition()));
            }
            children.add(terminal(Terminals.IDENTIFIER));
            lexer.advance();
        } else {
            throw new ParserException(String.format("%s expected: %d", Terminals.FROM, lexer.getCurrentPosition()));
        }
        return new Node(NonTerminals.FROM_CLAUSE, children);
    }

    /**
     * WHERE_CLAUSE -> where AND_EXPR | ε
     */
    private Node parseWhereClause(Lexer lexer) throws ParserException {
        ElementType current = lexer.getToken();
        List<Node> children = new ArrayList<>();
        if (current == Terminals.WHERE) {
            children.add(terminal(Terminals.WHERE));
            lexer.advance();
            children.add(parseAndExpr(lexer));
        } else if (current == Terminals.END) {
            return null;
        } else {
            throw new ParserException(String.format("%s or %s expected: %d", Terminals.WHERE, Terminals.END, lexer.getCurrentPosition()));
        }
        return new Node(NonTerminals.WHERE_CLAUSE, children);
    }

    /**
     * AND_EXPR -> COMPARE_EXPR AND_EXPR_1
     */
    private Node parseAndExpr(Lexer lexer) throws ParserException {
        ElementType current = lexer.getToken();
        List<Node> children = new ArrayList<>();
        if (current == Terminals.IDENTIFIER || current == Terminals.NUMBER) {
            children.add(parseCompareExpr(lexer));
            children.addAll(parseAndExpr1(lexer));
        } else {
            throw new ParserException(String.format("%s or %s expected: %d", Terminals.IDENTIFIER, Terminals.NUMBER, lexer.getCurrentPosition()));
        }
        return new Node(NonTerminals.AND_EXPR, children);
    }

    /**
     * private AND_EXPR_1 -> and_op COMPARE_EXPR AND_EXPR_1 | ε
     */
    private List<Node> parseAndExpr1(Lexer lexer) throws ParserException {
        ElementType current = lexer.getToken();
        List<Node> children = new ArrayList<>();
        if (current == Terminals.AND_OP) {
            children.add(terminal(Terminals.AND_OP));
            lexer.advance();
            children.add(parseCompareExpr(lexer));
            children.addAll(parseAndExpr1(lexer));
        } else if (current == Terminals.END) {
            return children;
        } else {
            throw new ParserException(String.format("%s or %s expected: %d", Terminals.AND_OP, Terminals.END, lexer.getCurrentPosition()));
        }
        return children;
    }

    /**
     * COMPARE_EXPR -> PRIMARY_EXPR COMPARE_EXPR_1
     */
    private Node parseCompareExpr(Lexer lexer) throws ParserException {
        ElementType current = lexer.getToken();
        List<Node> children = new ArrayList<>();
        if (current == Terminals.IDENTIFIER || current == Terminals.NUMBER) {
            children.add(parsePrimaryExpr(lexer));
            children.addAll(parseCompareExpr1(lexer));
        } else {
            throw new ParserException(String.format("%s or %s expected: %d", Terminals.IDENTIFIER, Terminals.NUMBER, lexer.getCurrentPosition()));
        }
        return new Node(NonTerminals.COMPARE_EXPR, children);
    }

    /**
     * PRIMARY_EXPR -> identifier | number
     */
    private Node parsePrimaryExpr(Lexer lexer) throws ParserException {
        ElementType current = lexer.getToken();
        List<Node> children = new ArrayList<>();
        if (current == Terminals.IDENTIFIER || current == Terminals.NUMBER) {
            children.add(terminal(current));
            lexer.advance();
        } else {
            throw new ParserException(String.format("%s or %s expected: %d", Terminals.IDENTIFIER, Terminals.NUMBER, lexer.getCurrentPosition()));
        }
        return new Node(NonTerminals.PRIMARY_EXPR, children);
    }

    /**
     * private COMPARE_EXPR_1 -> compare_op PRIMARY_EXPR COMPARE_EXPR_1 | ε
     */
    private List<Node> parseCompareExpr1(Lexer lexer) throws ParserException {
        ElementType current = lexer.getToken();
        List<Node> children = new ArrayList<>();
        if (current == Terminals.COMPARE_OP) {
            children.add(terminal(Terminals.COMPARE_OP));
            lexer.advance();
            children.add(parsePrimaryExpr(lexer));
            children.addAll(parseCompareExpr1(lexer));
        } else if (current == Terminals.AND_OP || current == Terminals.END) {
            return children;
        } else {
            throw new ParserException(String.format("%s or %s expected: %d", Terminals.COMPARE_OP, Terminals.END, lexer.getCurrentPosition()));
        }
        return children;
    }
}
