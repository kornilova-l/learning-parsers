package com.github.kornilova203;

/**
 * @author Liudmila Kornilova
 **/
public class ElementType {
    private final String name;

    public ElementType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
