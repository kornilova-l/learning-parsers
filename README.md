# Parsers study project
## LL(1) grammar

LL(1) parsers and Recursive descent parsers cannot parse left recursion.
So it has to be eliminated using following rule:
```
E → E + id | id

E → id E’
E’ → + id E’ | ε
```
 
```
QUERY            -> select COLUMN_LIST FROM_CLAUSE WHERE_CLAUSE
COLUMN_LIST      -> identifier COLUMN_LIST_TAIL
COLUMN_LIST_TAIL -> comma identifier COLUMN_LIST_TAIL | ε
FROM_CLAUSE      -> from identifier
WHERE_CLAUSE     -> where AND_EXPR | ε
AND_EXPR         -> COMPARE_EXPR AND_EXPR_1
AND_EXPR_1       -> and_op COMPARE_EXPR AND_EXPR_1 | ε
COMPARE_EXPR     -> PRIMARY_EXPR COMPARE_EXPR_1
COMPARE_EXPR_1   -> compare_op PRIMARY_EXPR COMPARE_EXPR_1 | ε
PRIMARY_EXPR     -> identifier | number
```

### FIRST and FOLLOW sets
|Production      |FIRST             |FOLLOW|
|----------------|------------------|---|
|QUERY           |select            |$|
|COLUMN_LIST     |identifier        |from|
|COLUMN_LIST_TAIL|comma, ε          |from|
|FROM_CLAUSE     |from              |where, $|
|WHERE_CLAUSE    |where, ε          |$|
|AND_EXPR        |identifier, number|$|
|AND_EXPR_1      |and_op, ε         |$|
|COMPARE_EXPR    |identifier, number|and_op, $|
|COMPARE_EXPR_1  |compare_op, ε     |and_op, $|
|PRIMARY_EXPR    |identifier, number|compare_op, and_op, $|

Number 1 in LL(1) means that parser needs to know only one terminal to decide that production rule to use.

### Parsing table
|                |select|identifier|comma|from |where|and_op|compare_op|number|$    |
|----------------|:----:|:--------:|:---:|:---:|:---:|:----:|:--------:|:----:|:---:|
|QUERY           |1     |          |     |     |     |      |          |      |     |
|COLUMN_LIST     |      |1         |     |     |     |      |          |      |     |
|COLUMN_LIST_TAIL|      |          |1    |ε    |     |      |          |      |     |
|FROM_CLAUSE     |      |          |     |1    |     |      |          |      |     |
|WHERE_CLAUSE    |      |          |     |     |1    |      |          |      |ε    |
|AND_EXPR        |      |1         |     |     |     |      |          |1     |     |
|AND_EXPR_1      |      |          |     |     |     |1     |          |      |ε    |
|COMPARE_EXPR    |      |1         |     |     |     |      |          |1     |     |
|COMPARE_EXPR_1  |      |          |     |     |     |ε     |1         |      |ε    |
|PRIMARY_EXPR    |      |1         |     |     |     |      |          |1     |     |

## SLR(1) grammar
Bottom-up parses can parse left recursion.

```
1     QUERY            -> select COLUMN_LIST FROM_CLAUSE WHERE_CLAUSE
2,3   COLUMN_LIST      -> COLUMN_LIST comma identifier | identifier
4     FROM_CLAUSE      -> from identifier
5,6   WHERE_CLAUSE     -> where AND_EXPR | ε
7,8   AND_EXPR         -> AND_EXPR and_op COMPARE_EXPR | COMPARE_EXPR
9,10  COMPARE_EXPR     -> COMPARE_EXPR compare_op PRIMARY_EXPR | PRIMARY_EXPR
11,12 PRIMARY_EXPR     -> identifier | number
```

### FIRST and FOLLOW sets
|Production      |FIRST             |FOLLOW|
|----------------|------------------|---|
|QUERY           |select            |$|
|COLUMN_LIST     |identifier        |comma, from|
|FROM_CLAUSE     |from              |where, $|
|WHERE_CLAUSE    |where, ε          |$|
|AND_EXPR        |identifier, number|and_op, $|
|COMPARE_EXPR    |identifier, number|and_op, compare_op, $|
|PRIMARY_EXPR    |identifier, number|and_op, compare_op, $|

### Canonical collection of LR(0) items

![Canonical collection of LR(0) items](img/canonical_collection.png)

## SLR(1) Parsing table

This grammar cannot be LR(0) grammar because of shift/reduce conflict in I15, I16, I18. Therefore, it's a SLR(1) grammar.
It means that we place `reduce` operations not below each terminal but instead only below terminals in FOLLOW set of this rule.

|      |select|identifier|comma|from |where|and_op|compare_op|number|$     |Q    |COLUMN_LIST  |FROM_CLAUSE|WHERE_CLAUSE|PRIMARY_EXPR|AND_EXPR|COMPARE_EXPR|
|------|:----:|:--------:|:---:|:---:|:---:|:----:|:--------:|:----:|:----:|:---:|:-----------:|:---------:|:----------:|:----------:|:------:|:----------:|
|I0    |S2    |          |     |     |     |      |          |      |      |1    |             |           |            |            |        |            |
|I1    |      |          |     |     |     |      |          |      |accept|     |             |           |            |            |        |            |
|I2    |      |S3        |     |     |     |      |          |      |      |     |4            |           |            |            |        |            |
|I3    |      |          |R3   |R3   |     |      |          |      |      |     |             |           |            |            |        |            |
|I4    |      |          |S7   |S5   |     |      |          |      |      |     |             |9          |            |            |        |            |
|I5    |      |S6        |     |     |     |      |          |      |      |     |             |           |            |            |        |            |
|I6    |      |          |     |     |R4   |      |          |      |R4    |     |             |           |            |            |        |            |
|I7    |      |S8        |     |     |     |      |          |      |      |     |             |           |            |            |        |            |
|I8    |      |          |R2   |R2   |     |      |          |      |      |     |             |           |            |            |        |            |
|I9    |      |          |     |     |S11  |      |          |      |R6    |     |             |           |10          |            |        |            |
|I10   |      |          |     |     |     |      |          |      |R1    |     |             |           |            |            |        |            |
|I11   |      |S13       |     |     |     |      |          |S14   |      |     |             |           |            |12          |16      |15          |
|I12   |      |          |     |     |     |R10   |R10       |      |R10   |     |             |           |            |            |        |            |
|I13   |      |          |     |     |     |R11   |R11       |      |R11   |     |             |           |            |            |        |            |
|I14   |      |          |     |     |     |R12   |R12       |      |R12   |     |             |           |            |            |        |            |
|I15   |      |          |     |     |     |R8    |S19       |      |R8    |     |             |           |            |            |        |            |
|I16   |      |          |     |     |     |S17   |          |      |R5    |     |             |           |            |            |        |            |
|I17   |      |S13       |     |     |     |      |          |S14   |      |     |             |           |            |12          |        |18          |
|I18   |      |          |     |     |     |R7    |S19       |      |R7    |     |             |           |            |            |        |            |
|I19   |      |S13       |     |     |     |      |          |S14   |      |     |             |           |            |20          |        |            |
|I20   |      |          |     |     |     |R9    |R9        |      |R9    |     |             |           |            |            |        |            |
